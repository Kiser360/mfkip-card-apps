const stream = require('stream');
const { promisify } = require('util');
const fs = require('fs');
const fss = require('fs-extra');
const got = require('got');
const { join } = require('path');
const { tmpdir } = require('os');

const pipeline = promisify(stream.pipeline);

async function downloadArtifact(artifactId, env, version) {
    const stagingDir = join(tmpdir(), 'mfkip');
    const artFilepath = join(stagingDir, 'artifacts', `${artifactId}.release.zip`);
    await fss.ensureDir(join(stagingDir, 'artifacts'));

    const url = `https://mfk.artifactory.asynchrony.com/artifactory/mfk-neutrino/${artifactId}/${env}/${version}/${artifactId}.release.zip`;
    console.log(`GET ARTIFACT: ${url}`);
    await pipeline(
        got.stream(url, {
            username: 'mfk-development',
            password: process.env.ARTIFACTORY_PASS,
            https: {
                rejectUnauthorized: false,
            },
        }),
        fs.createWriteStream(artFilepath)
    );

    process.on('exit', () => fss.removeSync(stagingDir));

    return artFilepath;
}

module.exports = {
    getCoreArtifact({ env, coreVersion }) {
        return downloadArtifact(`neutrino-core`, env, coreVersion || 'latest');
    },
    getBuildArtifact({ buildVersion }) {
        return downloadArtifact('neutrino-build', 'all', buildVersion || 'latest');
    },
    getAppArtifact(appId, env, version) {
        return downloadArtifact(`${appId}`, env, version);
    },
}
