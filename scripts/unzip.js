const unzipper = require('unzipper');
const fs = require('fs');

module.exports = function(zipSrc, outDir) {
    return new Promise((res, rej) => {
        const extracter = unzipper.Extract({ path: outDir });
        fs.createReadStream(zipSrc).pipe(extracter);
        extracter.once('close', res);
        extracter.once('error', rej);
    });
}
